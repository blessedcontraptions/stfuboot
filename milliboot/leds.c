#include <libopencm3/cm3/scb.h>

#include <libopencm3/stm32/f1/rcc.h>
#include <libopencm3/stm32/f1/flash.h>
#include <libopencm3/stm32/f1/gpio.h>
#include <libopencm3/stm32/f1/flash.h>
#include <libopencm3/stm32/desig.h>


#include <libopencm3/usb/usbd.h>

#include <stfuboot/initcall.h>


#define LED1_PIN 		GPIO7	/* PD7 green */
#define LED1_PORT 		GPIOD
#define LED2_PIN 		GPIO13	/* PD13 amber */
#define LED2_PORT 		GPIOD
#define LED3_PIN 		GPIO3	/* PD3 red */
#define LED3_PORT 		GPIOD
#define LED4_PIN 		GPIO4	/* PD4 blue */
#define LED4_PORT 		GPIOD


static int milliboot_clocks_init(void)
{
	/* usb clock */
        rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPDEN);

	return 0;
}
clock_initcall(milliboot_clocks_init);


static int milliboot_gpios_init(void)
{
	gpio_set_mode(LED1_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED1_PIN);
	gpio_set(LED1_PORT, LED1_PIN);

	gpio_set_mode(LED2_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED2_PIN);
	gpio_clear(LED2_PORT, LED2_PIN);

	gpio_set_mode(LED3_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED3_PIN);
	gpio_set(LED3_PORT, LED3_PIN);

	gpio_set_mode(LED4_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED4_PIN);
	gpio_clear(LED4_PORT, LED4_PIN);


	return 0;
}
gpio_initcall(milliboot_gpios_init);
