/*
 * This file is part of the stfuboot project.
 *
 * Copyright (C) 2012 Innovative Converged Devices (ICD)
 *
 * Author(s):
 *          Andrey Smirnov <andrey.smirnov@convergeddevices.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/cm3/vector.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/desig.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/stm32/f1/rcc.h>

#include <libopencm3/usb/usbd.h>
#include <libopencm3/usb/dfu.h>
#include <stfuboot/dfu.h>

#include <ccan/array_size.h>

#include <stfuboot/misc.h>
#include <stfuboot/initcall.h>


#define AS_ISTRING(x) ((x) + 3)
#define DFU_INTERFACE(num, func)					\
	{								\
		.bLength		= USB_DT_INTERFACE_SIZE,	\
			.bDescriptorType	= USB_DT_INTERFACE,	\
			.bInterfaceNumber	= 0,			\
			.bAlternateSetting	= num,			\
			.bNumEndpoints		= 0,			\
			.bInterfaceClass	= 0xFE,			\
			.bInterfaceSubClass	= 1,			\
			.bInterfaceProtocol	= 2,			\
			.iInterface		= AS_ISTRING(num),	\
			.extra			= &func,		\
			.extralen		= sizeof(func),		\
			}

static usbd_device *milliboot_usbddev;

static int milliboot_handle_control_request(usbd_device *udbddev, struct usb_setup_data *req, u8 **buf,
					    u16 *len, void (**complete)(usbd_device *udbddev, struct usb_setup_data *req))
{
	return milliboot_dfu_control_request(req, buf, len);
}


static int milliboot_clocks_init(void)
{
	rcc_peripheral_enable_clock(&RCC_AHBENR, RCC_AHBENR_OTGFSEN);
	return 0;
}
clock_initcall(milliboot_clocks_init);

static int milliboot_usb_init(void)
{
	static const struct usb_device_descriptor device_descriptor = {
		.bLength		= USB_DT_DEVICE_SIZE,
		.bDescriptorType	= USB_DT_DEVICE,
		.bcdUSB			= 0x0200,
		.bDeviceClass		= 0,
		.bDeviceSubClass	= 0,
		.bDeviceProtocol	= 0,
		.bMaxPacketSize0	= 64,
		.idVendor		= 0xCAFE,
		.idProduct		= 0xCAFE,
		.bcdDevice		= 0x0200,
		.iManufacturer		= 0,
		.iProduct		= 1,
		.iSerialNumber		= 2,
		.bNumConfigurations	= 1,
	};

	static u8 control_buffer[512];

	static const struct usb_dfu_descriptor dfu_descriptor = {
		.bLength		= sizeof(struct usb_dfu_descriptor),
		.bDescriptorType	= DFU_FUNCTIONAL,
		.bmAttributes		= USB_DFU_CAN_DOWNLOAD |
					  USB_DFU_CAN_UPLOAD |
		USB_DFU_WILL_DETACH,
		.wDetachTimeout		= 255,
		.wTransferSize		= sizeof(control_buffer),
		.bcdDFUVersion		= 0x0110,
	};

	static const struct usb_interface_descriptor interface_descriptors[] ={
		DFU_INTERFACE(0, dfu_descriptor),
		DFU_INTERFACE(1, dfu_descriptor),
		DFU_INTERFACE(2, dfu_descriptor),
	};

	static struct usb_interface interfaces[] = {
		{
			.num_altsetting = ARRAY_SIZE(interface_descriptors),
			.altsetting	= interface_descriptors,
		},
	};

	static struct usb_config_descriptor config = {
		.bLength		= USB_DT_CONFIGURATION_SIZE,
		.bDescriptorType	= USB_DT_CONFIGURATION,
		.wTotalLength		= 0,
		.bNumInterfaces		= 1,
		.bConfigurationValue	= 1,
		.iConfiguration		= 0,
		.bmAttributes		= 0xC0,
		.bMaxPower		= 0x32,

		.interface		= interfaces,
	};


	static char serial_number_string[30];
	static const char *usb_strings[] = {
		"Device with STFUBoot",
		serial_number_string,
		/* This string is used by ST Microelectronics' DfuSe utility. */
		"Application Firmware",
		"System Memory [0x1FFFB000 - 0x1FFFF7FF]",
		"Option Bytes [0x1FFFF800 - 0x1FFFF80F]",
	};

	desig_get_unique_id_as_string(serial_number_string,
				      sizeof(serial_number_string));

	/* FIXME: this code should automatically detect the type
	 * of MCU and use appropriate driver */
	milliboot_usbddev = usbd_init(&stm32f107_usb_driver,
				      &device_descriptor, &config,
				      usb_strings, ARRAY_SIZE(usb_strings),
				      control_buffer, sizeof(control_buffer));

	usbd_register_control_callback(milliboot_usbddev,
				       USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE,
				       USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT,
				       milliboot_handle_control_request);

	return 0;
}
post_initcall(milliboot_usb_init);

static int milliboot_usb_tick(void)
{
	usbd_poll(milliboot_usbddev);
	return 0;
}
state_machine_tickcall(milliboot_usb_tick);
