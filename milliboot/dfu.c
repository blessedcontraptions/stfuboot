/*
 * This file is part of the stfuboot project.
 *
 * Copyright (C) 2012 Innovative Converged Devices (ICD)
 *
 * Author(s):
 *          Andrey Smirnov <andrey.smirnov@convergeddevices.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>

#include <libopencm3/stm32/f1/flash.h>
#include <libopencm3/usb/usbd.h>

#include <stfuboot/initcall.h>
#include <stfuboot/dfu.h>
#include <stfuboot/infoblock.h>
#include <stfuboot/debug.h>

#define MIN(a, b) ((a) < (b) ? (a) : (b))


struct {
	enum dfu_status status;
	enum dfu_state  state;

	u8  bmAttributes;
	u32 wDetachTimeout;
	u32 wTransferSize;
} dfudev;


static int milliboot_dfu_dispatch_getstatus(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	(*buf)[0] = dfudev.status;
	(*buf)[1] = dfudev.wDetachTimeout & 0xFF;
	(*buf)[2] = (dfudev.wDetachTimeout >> 8) & 0xFF;
	(*buf)[3] = (dfudev.wDetachTimeout >> 16) & 0xFF;
	(*buf)[4] = dfudev.state;
	(*buf)[5] = 0; /* iString not used here */
	*len   = 6;

	return USBD_REQ_HANDLED;
}

static int milliboot_dfu_dispatch_getstate(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	(*buf)[0] = dfudev.state;
	*len   = 1;

	return USBD_REQ_HANDLED;
}

static int milliboot_dfu_dispatch_dnload_sync_getstate(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	/*
	  We are working in a polling mode and therefor block write
	  will always be complete by the time this function is called.
	 */
	dfudev.state = STATE_DFU_DNLOAD_IDLE;
	return milliboot_dfu_dispatch_getstatus(req, buf, len);
}

static int milliboot_dfu_write_buffer_to_flash(u32 writeptr, const u8 *buf, int len)
{
	int i, err = 0;

	flash_unlock();

	for (i = 0; i < len; i += 2) {
		u32 address;
		u16 data_in_flash, data_in_memory;

		address = writeptr + i;
		data_in_memory = *(u16 *)(buf + i);

		flash_program_half_word(address, data_in_memory);

		data_in_flash  = *(u16 *)address;

		if (data_in_flash != data_in_memory) {
			err = -EIO;
			break;
		}
	}

	flash_lock();
	return err;
}

static int milliboot_dfu_dispatch_dnload_idle_dnload(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	int write_len, i, err;

	static int total_size;

	/* FIXME proper support for multiple memory regions */
	const u8 *start_address  = (const u8 *)0x08005200;
	const u8 *end_address    = (const u8 *)(0x08000000 + 256 * 1024);
	const u8 *writeptr       = start_address + req->wValue * dfudev.wTransferSize;


	if (req->wValue == 0)
		total_size = 0;

	if (*len == 0) {
		/* FIXME: We need to figure some way of finding if all the FW
		 * has been transfered and check for it */
		if (0) {
			dfudev.status = DFU_STATUS_ERR_NOTDONE;
			dfudev.state  = STATE_DFU_ERROR;

			return USBD_REQ_NOTSUPP;
		} else {
			/*
			  All the Firmware have been transfered, start
			  calculating CRC and writing block info
			 */
			struct stfub_infoblock info;
			struct stfub_container container;

			stfub_code_blob_fill_info(&info.firmware,
						  (uint32_t)start_address,
						  (void *)start_address,
						  total_size);
			stfub_container_pack_and_sign(&container, &info, sizeof(info));

			err = milliboot_dfu_write_buffer_to_flash((u32)&_infoblock_offset,
								  (const u8 *)&container,
								  sizeof(container));
			if (err < 0)
				goto error;

			dfudev.state  = STATE_DFU_MANIFEST_SYNC;
			return USBD_REQ_HANDLED;
		}
	}

	write_len = MIN(*len, end_address - writeptr);

	/*
	   The following facts are true for this code:
	   	a) wTransferSize is even
		b) Size of ARM opcodes is always
		divisible at least by two(each instruction is 16
		bits in Thumb mode and 32 in non-Thumb)
		c) DFU suffix contains even number of bytes

	    Give a,b and c, in the event of write_len parameter being
	    odd it is reasonable to assume that, that something went
	    terribly wrong.
	*/
	if (write_len % 2)
		goto error;

	flash_unlock();
	/* FIXME: Adjust page size based on micro type */
	if (!req->wValue || !((u32)writeptr % 2048)) {
		/* Since we reserve first 512 byte for infoblock
		 * data, first time writeptr would have offset of
		 * 0x200, meaning that the criterion of divisibility
		 * by page size will not be satisfied. Nonetheless
		 * this page must be erased.

		 This peculiartiy happens because of the way the
		 firmware is flash: first all the code is written to
		 flash and after it is done infoblock gets transferd.
		 */
		flash_erase_page((u32)writeptr);
	}
	flash_lock();

	err = milliboot_dfu_write_buffer_to_flash((u32)writeptr, *buf, write_len);
	if (err < 0)
		goto error;

	total_size += write_len;

	dfudev.state = STATE_DFU_DNLOAD_SYNC;
	return USBD_REQ_HANDLED;

error:
	dfudev.status = DFU_STATUS_ERR_UNKNOWN;
	dfudev.state  = STATE_DFU_ERROR;
	return USBD_REQ_NOTSUPP;
}

static int milliboot_dfu_dispatch_idle_dnload(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	if (*len == 0 ||
	    !(dfudev.bmAttributes & USB_DFU_CAN_DOWNLOAD)) {
		dfudev.state = STATE_DFU_ERROR;
		return USBD_REQ_NOTSUPP;
	} else {
		return milliboot_dfu_dispatch_dnload_idle_dnload(req, buf, len);
	}
}

static int milliboot_dfu_dispatch_dnload_idle_abort(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	dfudev.state = STATE_DFU_IDLE;
	return USBD_REQ_HANDLED;
}

static int milliboot_dfu_dispatch_manifest_sync_getstatus(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	dfudev.state = STATE_DFU_IDLE;
	return milliboot_dfu_dispatch_getstatus(req, buf, len);
}

static int milliboot_dfu_dispatch_upload_idle_upload(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	int read_len;
	/* FIXME proper support for multiple memory regions */
	const u8 *start_address  = (const u8 *)0x08005200;
	const u8 *end_address    = (const u8 *)(0x08000000 + 256 * 1024);
	const u8 *readptr        = start_address + req->wValue * dfudev.wTransferSize;
	const int requested_size = *len;

	read_len = MIN(requested_size, end_address - readptr);

	memcpy(*buf, readptr, read_len);

	dfudev.state = (read_len < requested_size) ?
		STATE_DFU_IDLE : STATE_DFU_UPLOAD_IDLE;
	*len = read_len;

	return USBD_REQ_HANDLED;
}

static int milliboot_dfu_dispatch_idle_upload(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	if (dfudev.bmAttributes & USB_DFU_CAN_UPLOAD) {
		return milliboot_dfu_dispatch_upload_idle_upload(req, buf, len);
	} else {
		dfudev.state = STATE_DFU_ERROR;
		return USBD_REQ_NOTSUPP;
	}
}


static int milliboot_dfu_dispatch_upload_idle_abort(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	dfudev.state = STATE_DFU_IDLE;
	return USBD_REQ_HANDLED;
}

static int milliboot_dfu_dispatch_error_clrstatus(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	dfudev.status = DFU_STATUS_OK;
	dfudev.state  = STATE_DFU_IDLE;
	return USBD_REQ_HANDLED;
}

int milliboot_dfu_control_request(struct usb_setup_data *req, u8 **buf, u16 *len)
{
	int read_size, requested_size;

	_printf("%s [R:%d|S:%d]\n", __func__, req->bRequest, dfudev.state);

	if ((req->bmRequestType & 0x7F) != 0x21)
		return USBD_REQ_NOTSUPP; /* Only accept class request. */

	if (req->bRequest == DFU_GETSTATE &&
	    (dfudev.state != STATE_DFU_MANIFEST_WAIT_RESET ||
	     dfudev.state != STATE_DFU_DNBUSY ||
	     dfudev.state != STATE_DFU_MANIFEST))
		return milliboot_dfu_dispatch_getstate(req, buf, len);

	switch(dfudev.state) {
	/* Table A.2.3 of the official DFU spec v 1.1 */
	case STATE_DFU_IDLE:
		switch (req->bRequest) {
		case DFU_DNLOAD:
			return milliboot_dfu_dispatch_idle_dnload(req, buf, len);
		case DFU_UPLOAD:
			return milliboot_dfu_dispatch_idle_upload(req, buf, len);
		case DFU_ABORT:
			return USBD_REQ_HANDLED;
		case DFU_GETSTATUS:
			return milliboot_dfu_dispatch_getstatus(req, buf, len);
		default:
			break;
		}
		break;
	/* Table A.2.4 */
	case STATE_DFU_DNLOAD_SYNC:
		switch (req->bRequest) {
		case DFU_GETSTATUS:
			return milliboot_dfu_dispatch_dnload_sync_getstate(req, buf, len);
		default:
			break;
		}
		break;
	/* Table A.2.6 */
	case STATE_DFU_DNLOAD_IDLE:
		switch (req->bRequest) {
		case DFU_DNLOAD:
			return milliboot_dfu_dispatch_dnload_idle_dnload(req, buf, len);
		case DFU_ABORT:
			return milliboot_dfu_dispatch_dnload_idle_abort(req, buf, len);
		case DFU_GETSTATUS:
			return milliboot_dfu_dispatch_getstatus(req, buf, len);
		default:
			break;
		}
		break;
	/* Table A.2.7 */
	case STATE_DFU_MANIFEST_SYNC:
		switch (req->bRequest) {
		case DFU_GETSTATUS:
			return milliboot_dfu_dispatch_manifest_sync_getstatus(req, buf, len);
		default:
			break;
		}
		break;

	case STATE_DFU_UPLOAD_IDLE:
		switch (req->bRequest) {
		case DFU_UPLOAD:
			return milliboot_dfu_dispatch_upload_idle_upload(req, buf, len);
		case DFU_ABORT:
			return milliboot_dfu_dispatch_upload_idle_abort(req, buf, len);
		case DFU_GETSTATUS:
			return milliboot_dfu_dispatch_getstatus(req, buf, len);
		default:
			break;
		}
		break;

	case STATE_DFU_ERROR:
		switch (req->bRequest) {
		case DFU_GETSTATUS:
			return milliboot_dfu_dispatch_getstatus(req, buf, len);
		case DFU_CLRSTATUS:
			return milliboot_dfu_dispatch_error_clrstatus(req, buf, len);
		default:
			break;
		}
		break;

	default:
		break;
	}

	dfudev.status = DFU_STATUS_ERR_STALLEDPKT;
	dfudev.state  = STATE_DFU_ERROR;
	return USBD_REQ_NOTSUPP;
}

#if 0
static int milliboot_dfu_tick(void)
{
	switch(dfudev.state) {
	case STATE_DFU_DNBUSY:

		if (!queue_is_empty(&write_queue)) {
			firmware_block_t block;

			queue_dequeue(&write_queue, &block);
			if (milliboot_dfu_write_firmware_block(&block) < 0)  {
				dfudev.state = STATE_DFU_ERROR;
				break;
			}
		}

		dfudev.state = STATE_DFU_DNLOAD_SYNC;
		break;

	case STATE_DFU_MANIFEST:
		if (stfub_dfu_timeout_elapsed(&dfu)){
			if (stfub_dfu_attribute_is_set(&dfu, USB_DFU_MANIFEST_TOLERANT))
				stfub_dfu_set_state(&dfu, STATE_DFU_MANIFEST_SYNC);
			else
				stfub_dfu_set_state(&dfu, STATE_DFU_MANIFEST_WAIT_RESET);
		}
		break;

	default:
		break;
	}

	return 0;
}
state_machine_tickcall(milliboot_dfu_tick);
#endif

static int milliboot_dfu_init(void)
{
	dfudev.state	= STATE_DFU_IDLE;
	dfudev.status	= DFU_STATUS_OK;

	dfudev.bmAttributes = USB_DFU_CAN_DOWNLOAD |
		USB_DFU_CAN_UPLOAD |
		USB_DFU_WILL_DETACH;
	dfudev.wDetachTimeout = 100;
	dfudev.wTransferSize  = 512;
	
	return 0;
}
pre_initcall(milliboot_dfu_init);


#if 0
void stfub_dfu_switch_altsetting(usbd_device *usbd_dev, u16 interface, u16 altsetting)
{
	dfu.bank = &stfub_memory_banks[altsetting];
}


static int stfub_dfu_read_firmware_block(struct stfub_dfu *dfu, u16 block_no,
					 u8 *buf, int len)
{
	int read_len;
	const u8 *start_address  = (const u8 *)dfu->bank->start;
	const u8 *end_address    = (const u8 *)dfu->bank->end;

	if (block_no == 0)
		dfu->block.readptr = start_address;

	read_len = MIN(len, end_address - dfu->block.readptr);

	memcpy(buf, dfu->block.readptr, read_len);

	dfu->block.readptr += read_len;

	return read_len;
}


static bool stfub_dfu_attribute_is_set(struct stfub_dfu *dfu, u8 attribute)
{
	return dfu->descr->bmAttributes & attribute;
}

static enum dfu_state stfub_dfu_get_state(struct stfub_dfu *dfu)
{
	return dfu->state;
}

static void stfub_dfu_set_state(struct stfub_dfu *dfu,
				 enum dfu_state state)
{
	dfu->state = state;
}

static enum dfu_status stfub_dfu_get_status(struct stfub_dfu *dfu)
{
	return dfu->status;
}

static void stfub_dfu_set_status(struct stfub_dfu *dfu,
				  enum dfu_status status)
{
	dfu->status = status;
}

static u32 stfub_dfu_get_poll_timeout(struct stfub_dfu *dfu)
{
	return dfu->timeout;
}

static bool stfub_dfu_timeout_elapsed(struct stfub_dfu *dfu)
{
	/* FIXME: Implement proper timeout support */
	return true;
}

static int stfub_dfu_handle_get_status_request(struct stfub_dfu *dfu, u8 *buf,
						u16 *len)
{
	u32 timeout = stfub_dfu_get_poll_timeout(dfu);

	buf[0] = stfub_dfu_get_status(dfu);
	buf[1] = timeout & 0xFF;
	buf[2] = (timeout >> 8) & 0xFF;
	buf[3] = (timeout >> 16) & 0xFF;
	buf[4] = stfub_dfu_get_state(dfu);
	buf[5] = 0; /* iString not used here */
	*len   = 6;

	return USBD_REQ_HANDLED;
}

static int stfub_dfu_handle_get_state_request(struct stfub_dfu *dfu, u8 *buf,
					       u16 *len)
{
	buf[0] = stfub_dfu_get_state(dfu);
	*len   = 1;

	return USBD_REQ_HANDLED;
}

static void stfub_dfu_timestamp_poll_request(struct stfub_dfu *dfu)
{
}

static int stfub_dfu_queue_firmware_block(struct stfub_dfu *dfu,
					  u16 block_no, const u8 *buf,
					  int len)
{

	if ((unsigned int) len > sizeof(dfu->pending.block))
		return -1;

	dfu->pending.block_no = block_no;
	memcpy(dfu->pending.block, buf, len);

	dfu->pending.block_len = len;

	return 0;
}

static bool stfub_dfu_write_pending(struct stfub_dfu *dfu)
{
	return dfu->pending.block_len != -1;
}


static bool dfu_all_data_is_received(struct stfub_dfu *dfu)
{
	return true;
}




#endif
