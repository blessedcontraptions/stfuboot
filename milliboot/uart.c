#include <stdio.h>
#include <stdbool.h>
#include <errno.h>

#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/f1/gpio.h>
#include <libopencm3/stm32/f1/rcc.h>
#include <libopencm3/stm32/f1/nvic.h>

#include <stfuboot/gqueue.h>
#include <stfuboot/misc.h>
#include <stfuboot/initcall.h>

DECLARE_QUEUE(char, uart_tx_buf, 256);
DECLARE_QUEUE(char, uart_rx_buf, 256);

int _putchar(char c)
{
	if(c == '\n')
		_putchar('\r');
	if(!queue_is_full(&uart_tx_buf)) {
		queue_enqueue(&uart_tx_buf, &c);
	} else {
		return -ENOMEM;
	}

	usart_enable_tx_interrupt(USART2);

	return 0;
}

#if 0
int getchar(char *c)
{
	if(!queue_is_empty(&uart_rx_buf)) {
		queue_dequeue(&uart_rx_buf, c);
		return 0;
	} else {
		return -ENODATA;
	}
}
#endif

static int milliboot_uart_init(void)
{
	usart_set_baudrate(USART2, 115200);
	usart_set_databits(USART2, 8);
	usart_set_parity(USART2, USART_PARITY_NONE);
	usart_set_flow_control(USART2, USART_FLOWCONTROL_NONE);
	usart_set_stopbits(USART2, USART_CR2_STOPBITS_1);
	usart_set_mode(USART2, USART_MODE_TX_RX);
	usart_enable_rx_interrupt(USART2);
	usart_enable(USART2);

	return 0;
}
post_initcall(milliboot_uart_init);

static int milliboot_uart_irq_init(void)
{
	nvic_enable_irq(NVIC_USART2_IRQ);
	nvic_set_priority(NVIC_USART2_IRQ, 3);

	return 0;
}
irq_initcall(milliboot_uart_irq_init);

static int milliboot_gpio_init(void)
{
	gpio_primary_remap(AFIO_MAPR_SWJ_CFG_FULL_SWJ,
			   AFIO_MAPR_USART2_REMAP);

	gpio_set_mode(GPIOD, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO5);
	gpio_set_mode(GPIOD, GPIO_MODE_INPUT,
		      GPIO_CNF_INPUT_FLOAT, GPIO6);

	return 0;
}
gpio_initcall(milliboot_gpio_init);

static int milliboot_clock_init(void)
{
	rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_USART2EN);
        rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_AFIOEN);

	return 0;
}
clock_initcall(milliboot_clock_init);


static void milliboot_usart2_isr(void)
{
	char c;

	if (usart_get_flag(USART2, USART_SR_TXE)) {
		if (!queue_is_empty(&uart_tx_buf)) {
			queue_dequeue(&uart_tx_buf, &c);
			usart_send(USART2, (uint16_t)c);
		} else {
			usart_disable_tx_interrupt(USART2);
		}
	}

	if (usart_get_flag(USART2, USART_SR_RXNE)) {
		if (!queue_is_full(&uart_rx_buf)) {
			c = usart_recv(USART2);
			queue_enqueue(&uart_rx_buf, &c);
		}
	}
}

void usart2_isr(void) ALIAS(milliboot_usart2_isr);
