/*
 * This file is part of the stfuboot project.
 *
 * Copyright (C) 2012 Innovative Converged Devices (ICD)
 *
 * Author(s):
 *          Andrey Smirnov <andrey.smirnov@convergeddevices.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/f1/rcc.h>

#include <stfuboot/misc.h>
#include <stfuboot/initcall.h>
#include <stfuboot/debug.h>


#define EOHFUCK 42

int main(void)
{
	int result;
	initcall_t *initcall, *tickcall;

	/*
	   FIXME: This is only workable on STM32F107 devkit with 25mhz crystall
	 */
	rcc_clock_setup_in_hse_25mhz_out_72mhz();

	for (initcall = __initcalls_start;
	     initcall < __initcalls_end; initcall++) {
		result = (*initcall)();
		if (result < 0)
			return result;
	}

	_printf("[milliboot] Done initializing peripherals\n");

	for (;;) {
		for (tickcall = __ticks_start;
		     tickcall < __ticks_end; tickcall++) {
			result = (*tickcall)();
			if (result < 0)
				return result;
		}
	}

	return -EOHFUCK;
}


__attribute__ ((naked, noreturn, interrupt))
void milliboot_reset_handler(void)
{
	volatile unsigned *src, *dest;

	extern unsigned _data_loadaddr;
	extern unsigned _data;
	extern unsigned _edata;
	extern unsigned _ebss;

	/* Copy .data section to system RAM */
	for (src = &_data_loadaddr, dest = &_data; dest < &_edata; src++, dest++)
		*dest = *src;

	while (dest < &_ebss)
		*dest++ = 0;

	/* Call the application's entry point. */
	if(!main()) {
		/*
		   Main finished succesfully, reset the controller
		 */
		stfub_start_with_vector_table_at_offset(0x00000000);
	} else {
		/*
		   For some reason main returnes negative error code.
		   Go to system bootloader in that case
		 */
		stfub_start_with_vector_table_at_offset(&_system_memory_offset);
	}
}

void reset_handler(void)	ALIAS(milliboot_reset_handler);
void hard_fault_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void mem_manage_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void bus_fault_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void usage_fault_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void wwdg_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void pvd_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tamper_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void rtc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void flash_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void rcc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti0_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti4_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel1_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel2_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel3_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel4_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel5_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel6_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel7_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void adc1_2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void usb_hp_can_tx_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void usb_lp_can_rx0_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void can_rx1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can_sce_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti9_5_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_brk_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_up_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_trg_com_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_cc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim4_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c1_ev_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c1_er_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c2_ev_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c2_er_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void spi1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void spi2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void usart1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
/* void usart2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader); */
void usart3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti15_10_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void rtc_alarm_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void usb_wakeup_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_brk_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_up_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_trg_com_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_cc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void adc3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void fsmc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void sdio_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim5_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void spi3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void uart4_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void uart5_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim6_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim7_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel1_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel2_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel3_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel4_5_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel5_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void eth_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void eth_wkup_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_tx_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_rx0_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_rx1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_sce_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void otg_fs_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
