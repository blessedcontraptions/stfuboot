/*
 * This file is part of the libopencm3 project.
 *
 * Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 * Copyright (C) 2011 Damjan Marion <damjan.marion@gmail.com>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <libopencm3/stm32/f1/rcc.h>
#include <libopencm3/stm32/f1/gpio.h>
#include <libopencm3/stm32/exti.h>
#include <libopencm3/stm32/f1/nvic.h>
#include <libopencm3/stm32/pwr.h>
#include <libopencm3/stm32/f1/bkp.h>



#define LED1_PIN 		GPIO7	/* PD7 green */
#define LED1_PORT 		GPIOD
#define LED2_PIN 		GPIO13	/* PD13 amber */
#define LED2_PORT 		GPIOD
#define LED3_PIN 		GPIO3	/* PD3 red */
#define LED3_PORT 		GPIOD
#define LED4_PIN 		GPIO4	/* PD4 blue */
#define LED4_PORT 		GPIOD

static void irq_setup(void)
{
	exti_select_source(EXTI9, GPIOB);
	exti_set_trigger(EXTI9, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI9);
	nvic_enable_irq(NVIC_EXTI9_5_IRQ);
}

/* Set STM32 to 72 MHz. */
static void clock_setup(void)
{
	rcc_clock_setup_in_hse_25mhz_out_72mhz();
        rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPDEN);
        rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_IOPBEN);
	rcc_peripheral_enable_clock(&RCC_APB2ENR, RCC_APB2ENR_AFIOEN);
}

static void gpio_setup(void)
{
	/* Set GPIO6/7 (in GPIO port C) to 'output push-pull'. */
	gpio_set_mode(LED1_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED1_PIN);
	gpio_set(LED1_PORT, LED1_PIN);

	gpio_set_mode(LED2_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED2_PIN);
	gpio_clear(LED2_PORT, LED2_PIN);

	gpio_set_mode(LED3_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED3_PIN);
	gpio_set(LED3_PORT, LED3_PIN);

	gpio_set_mode(LED4_PORT, GPIO_MODE_OUTPUT_50_MHZ,
		      GPIO_CNF_OUTPUT_PUSHPULL, LED4_PIN);
	gpio_clear(LED4_PORT, LED4_PIN);

	gpio_set_mode(GPIOB, GPIO_MODE_INPUT,
			      GPIO_CNF_INPUT_FLOAT, GPIO9);

}

int main(void)
{
	int i;

	clock_setup();
	gpio_setup();
	irq_setup();

	/* Blink the LEDs (PC6 and PC7) on the board. */
	while (1) {
		gpio_toggle(LED1_PORT, LED1_PIN);
		gpio_toggle(LED2_PORT, LED2_PIN);
		/* gpio_toggle(LED3_PORT, LED3_PIN); */
		/* gpio_toggle(LED4_PORT, LED4_PIN); */

 		for (i = 0; i < 8000000; i++)	/* Wait a bit. */
			__asm__("nop");
	}

	return 0;
}

void exti9_5_isr(void)
{
	rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_PWREN);
	rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_BKPEN);

	pwr_disable_backup_domain_write_protect();

	BKP_DR2 = 0x4242;

	gpio_clear(LED3_PORT, LED3_PIN);
	exti_reset_request(EXTI9);
}
