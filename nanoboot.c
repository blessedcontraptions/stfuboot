/*
 * This file is part of the stfuboot project.
 *
 * Copyright (C) 2012 Innovative Converged Devices (ICD)
 *
 * Author(s):
 *          Andrey Smirnov <andrey.smirnov@convergeddevices.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>


#include <libopencm3/cm3/vector.h>
#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/f1/rcc.h>
#include <libopencm3/stm32/f1/bkp.h>
#include <libopencm3/stm32/f1/flash.h>
#include <libopencm3/stm32/dbgmcu.h>
#include <libopencm3/stm32/pwr.h>


#include <stfuboot/carveout.h>
#include <stfuboot/container.h>
#include <stfuboot/misc.h>
#include <stfuboot/flashwp.h>

#define NANOBOOT_MAX_WP_ATTEMPTS (4)

static void nanoboot_bootloop_counter_increment(void)
{
	rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_PWREN);
	rcc_peripheral_enable_clock(&RCC_APB1ENR, RCC_APB1ENR_BKPEN);

	pwr_disable_backup_domain_write_protect();

	++BKP_DR1;
}

static u16 nanoboot_bootloop_counter_value(void)
{
	return BKP_DR1;
}

__attribute__ ((interrupt, naked, noreturn))
void nanoboot_reset_handler(void)
{
	struct stfub_carveout *carveout = stfub_get_carveout();

	if (carveout && stfub_code_blob_is_valid(&carveout->microboot))  {
		/* 
		   Before transfering control to the code of the next
		   stage bootloader wnsure that the areas of flash
		   corresponding to this bootloader are write protected
		 */

		/* It appears that in some cases the
		 * 'stfub_protect_pages_and_reset' call can fail
		 * miserably which would cause a bootloop to appear.
		 * To prevent this a boot counter in one of the
		 * backup registers is used if we try to change
		 * option bytes more than 4 times booloader will jump
		 * to microboot regardless
		 */
		nanoboot_bootloop_counter_increment();
		if ((~FLASH_WRPR & FLASH_WRP_PAGE(0)) ||
		    (nanoboot_bootloop_counter_value() > NANOBOOT_MAX_WP_ATTEMPTS))
			stfub_start_with_vector_table_at_offset((void *)carveout->microboot.offset);
		else
			stfub_protect_pages_and_reset(FLASH_WRP_PAGE(0));
	} else {
		stfub_start_with_vector_table_at_offset(&_system_memory_offset);
	}
}

void reset_handler(void)	ALIAS(nanoboot_reset_handler);
void hard_fault_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void mem_manage_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void bus_fault_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void usage_fault_handler(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void wwdg_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void pvd_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tamper_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void rtc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void flash_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void rcc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti0_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti4_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel1_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel2_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel3_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel4_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel5_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel6_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma1_channel7_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void adc1_2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void usb_hp_can_tx_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void usb_lp_can_rx0_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void can_rx1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can_sce_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti9_5_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_brk_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_up_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_trg_com_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void tim1_cc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim4_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c1_ev_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c1_er_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c2_ev_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void i2c2_er_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void spi1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void spi2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void usart1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void usart2_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void usart3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void exti15_10_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void rtc_alarm_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void usb_wakeup_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_brk_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_up_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_trg_com_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void tim8_cc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void adc3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void fsmc_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void sdio_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim5_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void spi3_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void uart4_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void uart5_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim6_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void tim7_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel1_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel2_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel3_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel4_5_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void dma2_channel5_isr(void)	ALIAS(stfub_jump_to_maskrom_bootloader);
void eth_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void eth_wkup_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_tx_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_rx0_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_rx1_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void can2_sce_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
void otg_fs_isr(void)		ALIAS(stfub_jump_to_maskrom_bootloader);
