/*
 * This file is part of the stfuboot project.
 *
 * 	Copyright (C) 2012 Innovative Converged Devices (ICD)
 *
 * Author:
 *	Andrey Smirnov <andrey.smirnov@convergeddevices.net>
 *
 * Based on a simlar linker script form libopencm3 project
 *
 *	Copyright (C) 2009 Uwe Hermann <uwe@hermann-uwe.de>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */


#if defined CONFIG_STM32F107xCxx
#include "stm32f107xcxx.ld"
#endif
#if defined CONFIG_STM32F103xBxx
#include "stm32f103xbxx.ld"
#endif

PROVIDE(_ram_start		= ORIGIN(ram));
PROVIDE(_ram_end		= ORIGIN(ram) + LENGTH(ram));
PROVIDE(_stack			= _ram_end);
PROVIDE(_carveout_offset	= ORIGIN(carveout));
PROVIDE(_infoblock_offset	= ORIGIN(infoblock));
PROVIDE(_firmware_start		= ORIGIN(firmware));
PROVIDE(_system_memory_offset	= ORIGIN(system_memory));
PROVIDE(_option_bytes_start	= ORIGIN(option_bytes));
PROVIDE(_option_bytes_end	= ORIGIN(option_bytes) + LENGTH(option_bytes));

