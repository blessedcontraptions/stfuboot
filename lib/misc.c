#include <libopencm3/cm3/vector.h>
#include <libopencm3/cm3/scb.h>

#include <stfuboot/memory-layout.h>

__attribute__ ((naked, noreturn))
void stfub_start_with_vector_table_at_offset(void *offset)
{
	vector_table_t *vtable = offset;

	__asm__ __volatile__ ("msr msp, %0" : : "r"(vtable->initial_sp_value));
	SCB_VTOR = (u32)vtable;
	vtable->reset();

	for (;;) {}		
}
