#include <string.h>
#include <ccan/crc.h>

#include <stfuboot/container.h>


void stfub_container_pack_and_sign(struct stfub_container *container,
				   void *blob, uint32_t blob_size);

void stfub_container_pack_and_sign(struct stfub_container *container,
				   void *blob, uint32_t blob_size)
{
	memcpy(container->blob, blob, blob_size);
	memset(container->blob + blob_size, 42, sizeof(container->blob) - blob_size);

	container->crc = crc32c(0, container->blob, sizeof(container->blob));
}

static bool stfub_container_is_intact(const struct stfub_container *container)
{
	uint32_t crc;

	crc = crc32c(0, container->blob, sizeof(container->blob));

	return crc == container->crc;
}

void *stfub_container_unpack(struct stfub_container *container)
{
	if (!stfub_container_is_intact(container))
		return NULL;

	return (void *)container->blob;
}

bool stfub_code_blob_is_valid(struct stfub_code_blob_info *info)
{
	uint32_t crc;

	crc = crc32c(0, (const void *)info->offset, info->size);

	return crc == info->crc;
}

void stfub_code_blob_fill_info(struct stfub_code_blob_info *info, 
			       uint32_t offset, void *code, uint32_t code_size)
{
	info->offset = offset;
	info->size   = code_size;
	info->crc    = crc32c(0, code, code_size);
}
