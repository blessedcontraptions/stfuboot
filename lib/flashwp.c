#include <string.h>

#include <libopencm3/cm3/scb.h>
#include <libopencm3/stm32/f1/flash.h>

#include <stfuboot/memory-layout.h>

__attribute__ ((noreturn))
void stfub_protect_pages_and_reset(u8 mask)
{
	/*
	  Fixme what would happen if Vcc dissapears in
	  the middle of this code's execution
	*/

	struct option_bytes *ob = (struct option_bytes *)&_option_bytes_start;

	flash_unlock();
	flash_program_option_bytes((u32)&ob->wrp0, ob->wrp0 & (~mask));
	flash_lock();

	scb_reset_system();
}
