#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <ccan/crc.h>
#include <stfuboot/container.h>
#include <stfuboot/carveout.h>

static void fill_blob_info_for_file(const char *name,
				    struct stfub_code_blob_info *info,
				    uint32_t offset)
{
	FILE *bootstep = fopen(name, "r");

	uint8_t *data;
	uint32_t size;

	fseek(bootstep, 0L, SEEK_END);
	size = ftell(bootstep);
	fseek(bootstep, 0L, SEEK_SET);

	data = malloc(size);
	fread(data, size, sizeof(uint8_t), bootstep);
	fclose(bootstep);

	stfub_code_blob_fill_info(info, offset, data, size);
	free(data);
}

int main(int argc, char **argv)
{
	int i;

	struct stfub_carveout carveout;
	struct stfub_container container;

	FILE *file	= fopen("carveout.bin", "w");

	fill_blob_info_for_file("microboot.bin", &carveout.microboot, 0x08001000);
	fill_blob_info_for_file("milliboot.bin", &carveout.milliboot, 0x08002200);

	stfub_container_pack_and_sign(&container, &carveout, sizeof(carveout));
	
	fwrite(&container, sizeof(container), 1, file);

	fclose(file);

	return 0;
}
