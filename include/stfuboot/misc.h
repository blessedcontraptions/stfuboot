#ifndef __MISC_H__
#define __MISC_H__


#include <libopencm3/cm3/vector.h>
#include <libopencm3/cm3/scb.h>

#include "memory-layout.h"

#define ALIAS(name) __attribute__((alias (#name)))



void stfub_start_with_vector_table_at_offset(void *offset)
	__attribute__ ((naked, noreturn));

__attribute__ ((naked, noreturn, interrupt))
static void stfub_jump_to_maskrom_bootloader(void)
{
	register vector_table_t *vtable;
	register struct scb_exception_stack_frame *frame;

	SCB_GET_EXCEPTION_STACK_FRAME(frame);

	vtable		= (vector_table_t *)&_system_memory_offset;
	SCB_VTOR	= (u32)vtable;
	frame->pc	= (u32)vtable->reset;

	asm volatile ("bx lr");
}


#endif /* __MISC_H__ */
