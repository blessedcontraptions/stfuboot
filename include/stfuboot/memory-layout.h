#ifndef __STFUBOOT_MEMORY_LAYOUT__
#define __STFUBOOT_MEMORY_LAYOUT__

extern unsigned _ram_start;
extern unsigned _ram_end;
extern unsigned _stack;
extern unsigned _system_memory_offset;
extern unsigned _option_bytes_offset;
extern unsigned _carveout_offset;
extern unsigned _infoblock_offset;
extern unsigned _firmware_start;
extern unsigned _option_bytes_start, _option_bytes_end;

#endif	/* __STFUBOOT_MEMORY_LAYOUT__ */
