#ifndef __CARVEOUT_H__
#define __CARVEOUT_H__

#include <stdint.h>
#include <stdbool.h>

/* 
   A simple carveout that stores a bunch of information about
   different stages of the bootloader. It is stored in 512 bytes of flash
   that are located In between micro and milli bootloaders
 */


#include <stfuboot/container.h>

struct stfub_carveout {
	struct stfub_code_blob_info microboot;
	struct stfub_code_blob_info milliboot;
} __attribute__((packed));


#if defined STM32F1

#include <stfuboot/memory-layout.h>


static inline struct stfub_carveout *stfub_get_carveout(void)
{
	return (struct stfub_carveout *)
		stfub_container_unpack((struct stfub_container *)&_carveout_offset);
}

#endif

#endif	/* __CARVEOUT_H__ */
