#ifndef __STFUB_CONTAINER_H__
#define __STFUB_CONTAINER_H__

#include <stdint.h>
#include <stdbool.h>

struct stfub_code_blob_info {
	uint32_t offset;
	uint32_t size;
	uint32_t crc;
} __attribute__((packed));

struct stfub_container {
	uint8_t blob[512 - sizeof(uint32_t)];
	uint32_t crc;
}  __attribute__((packed));

void stfub_container_pack_and_sign(struct stfub_container *container,
				   void *blob, uint32_t blob_size);
void *stfub_container_unpack(struct stfub_container *container);
bool stfub_code_blob_is_valid(struct stfub_code_blob_info *info);
void stfub_code_blob_fill_info(struct stfub_code_blob_info *info, 
			       uint32_t offset, void *code, uint32_t code_size);


#endif
