#ifndef __INFOBLOCK_H__
#define __INFOBLOCK_H__

#include <stdint.h>
#include <stdbool.h>

#include <stfuboot/container.h>

struct stfub_infoblock {
	struct stfub_code_blob_info firmware;
} __attribute__((packed));


#include <stfuboot/memory-layout.h>


static inline struct stfub_infoblock *stfub_get_infoblock(void)
{
return (struct stfub_infoblock *)stfub_container_unpack((struct stfub_container *)&_infoblock_offset);
}



#endif	/* __INFOBLOCK_H__ */
