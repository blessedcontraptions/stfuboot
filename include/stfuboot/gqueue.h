#ifndef _QUEUE_H_
#define _QUEUE_H_

#include <string.h>
#include <stdint.h>
#include <stdbool.h>

struct generic_queue {
	volatile void *head;
	volatile void *tail;
	size_t item_size;
	size_t len;
	size_t max_capacity;
	volatile uint8_t memory[0];
};

#define DECLARE_QUEUE(element_type, name, max_size)		\
        struct name {                                           \
                struct generic_queue gq;                        \
                element_type __elements[max_size];              \
        } name = {                                              \
                .gq={                                           \
                        .len		= 0,                    \
                        .item_size	= sizeof(element_type), \
                        .max_capacity	= max_size,             \
                },                                              \
        }


static inline bool queue_is_empty(volatile void *q)
{
	volatile struct generic_queue *gq = q;

	return (gq->len == 0);
}
static inline int queue_get_len(volatile void *q)
{
	volatile struct generic_queue *gq = q;
	return gq->len;
}
static inline bool queue_is_full(volatile void *q)
{
	volatile struct generic_queue *gq = q;
	return (gq->len >= gq->max_capacity);
}
void queue_enqueue(volatile void *q, const void *elt) __attribute__((nonnull));
void queue_dequeue(volatile void *q, void *elt) __attribute__((nonnull));

#endif
