#ifndef __INITCALL_H__
#define __INITCALL_H__


typedef int (*initcall_t)(void);

#define __define_initcall(level,fn,id) \
	static initcall_t __initcall_##fn##id __attribute__((__used__)) \
	__attribute__((__section__(".initcall." level))) = fn

#define pre_initcall(fn)		__define_initcall("0",fn,0)
#define clock_initcall(fn)		__define_initcall("1",fn,1)
#define gpio_initcall(fn)		__define_initcall("2",fn,2)
#define irq_initcall(fn)		__define_initcall("3",fn,3)
#define post_initcall(fn)		__define_initcall("4",fn,4)

#define state_machine_tickcall(fn)	__define_initcall("5",fn,5)


extern initcall_t __initcalls_start[], __initcalls_end[];
extern initcall_t __ticks_start[], __ticks_end[];

#endif
