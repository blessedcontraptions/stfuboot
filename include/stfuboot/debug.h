#ifndef __DEBUG_H__
#define __DEBUG_H__

#include <stfuboot/printf.h>



int _printf(const char *format, ...);
/* #define debug(fmt, arg) _printf(fmt, ##arg) */

#endif
