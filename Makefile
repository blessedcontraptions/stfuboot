#
# This file is part of the stfuboot project.
#
# Copyright (C) 2012 Innovative Converged Devices(ICD)
#
# Author(s):
# 	Andrey Smirnov <andrey.smirnov@convergeddevices.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


ifeq ($(wildcard .config),)
$(error Cannot find .config - please run make <something>config first)
else
-include .config
endif

ifeq ($(wildcard libopencm3),)
$(error Cannot find symlink for libopencm3. Bulid will most certainly fail)
endif

# Be silent per default, but 'make V=1' will show all compiler calls.
ifneq ($(V),1)
Q := @
# Do not print "Entering directory ...".
MAKEFLAGS += --no-print-directory
endif


PREFIX	?= arm-none-eabi
CC 	:= $(PREFIX)-gcc
CPP 	:= $(PREFIX)-cpp



CFLAGS := -Os -g -Waddress -Warray-bounds -Wchar-subscripts -Wenum-compare 	\
          -Wimplicit-int -Wimplicit-function-declaration -Wcomment 		\
          -Wformat -Wmain -Wmissing-braces -Wnonnull -Wparentheses		\
	  -Wpointer-sign -Wreturn-type -Wsequence-point -Wsign-compare		\
	  -Wstrict-aliasing -Wstrict-overflow=1 -Wswitch -Wtrigraphs		\
	  -Wuninitialized -Wunknown-pragmas -Wunused-function -Wunused-label	\
	  -Wunused-value -Wvolatile-register-var -Wextra -fno-common -mthumb	\
	  -mcpu=cortex-m3 -msoft-float -MD

CFLAGS += -DSTM32F1 -Ilibopencm3/include -Iinclude

LDFLAGS ?= -Wl,--start-group -Wl,--end-group 	\
	   -nostartfiles -Wl,--gc-sections -mthumb -mcpu=cortex-m3 -msoft-float

LIBS = -Llibopencm3/lib -lopencm3_stm32f1


cppflags-y = -P -C
cppflags-$(CONFIG_STM32F107xCxx) += -DCONFIG_STM32F107xCxx
cppflags-$(CONFIG_STM32F103xBxx) += -DCONFIG_STM32F103xBxx

COMMON_LNK = linker/memory-layout.lds

all: nanoboot.bin microboot.bin milliboot.bin

linker/memory-layout.lds: linker/memory-layout.ld.S
	$(Q)$(CPP) $(cppflags-y) $< > $@

# Configuration targets
#
# They depend on a kconfig-frontends package. In order to invoke them
# you need to download and install kconfig-frontends from
# http://ymorin.is-a-geek.org/projects/kconfig-frontends

config:
	kconfig-conf Kconfig
oldconfig:
	kconfig-conf --oldconfig Kconfig
menuconfig:
	kconfig-mconf Kconfig


%.o: %.c
	@printf "  CC      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) $(CFLAGS) -o $@ -c $<

%.bin: %.elf
	@printf "  OBJCOPY $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(PREFIX)-objcopy -Obinary $< $@

# ==========================================================
# Nanoboot
# ==========================================================

NANOBOOT-OBJS := nanoboot.o lib/misc.o lib/crc.o lib/container.o lib/flashwp.o
NANOBOOT-LNK  := linker/nanoboot.ld $(COMMON_LNK)

nanoboot.elf: $(NANOBOOT-OBJS) $(NANOBOOT-LNK)
	@printf "  LD      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) -o $@ -T linker/nanoboot.ld $(LDFLAGS) $(NANOBOOT-OBJS) $(LIBS)

# ==========================================================
# Microboot
# ==========================================================

MICROBOOT-OBJS := microboot.o lib/misc.o lib/crc.o lib/container.o lib/flashwp.o
MICROBOOT-LNK  := linker/microboot.ld $(COMMON_LNK)

microboot.elf: $(MICROBOOT-OBJS) $(MICROBOOT-LNK)
	@printf "  LD      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) -o $@ -T linker/microboot.ld $(LDFLAGS) $(MICROBOOT-OBJS) $(LIBS)

# ==========================================================
# Milliboot
# ==========================================================

MILLIBOOT-OBJS := milliboot/main.o milliboot/usb.o milliboot/dfu.o milliboot/uart.o milliboot/leds.o lib/misc.o lib/crc.o lib/container.o lib/gqueue.o lib/printf.o
MILLIBOOT-LNK  := linker/milliboot.ld $(COMMON_LNK)

milliboot.elf: $(MILLIBOOT-OBJS) $(MILLIBOOT-LNK)
	@printf "  LD      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) -o $@ -T linker/milliboot.ld $(LDFLAGS) $(MILLIBOOT-OBJS) $(LIBS)

# ==========================================================
# Carveout
# ==========================================================
gen-carveout:
	$(Q)make -C tools/

carveout.bin: microboot.bin milliboot.bin gen-carveout
	$(Q)tools/gen-carveout

# ==========================================================
# Test
# ==========================================================
FANCYBLINK-LNK := test/fancyblink.ld $(COMMON_LNK)

fancyblink.elf: test/fancyblink.o $(FANCYBLINK-LNK)
	@printf "  LD      $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(CC) -o $@ -T test/fancyblink.ld $(LDFLAGS) test/fancyblink.o $(LIBS)

flash_fancyblink:
	sudo dfu-util -i0 -a0 -D fancyblink.bin -d cafe:cafe


ifeq ($(STM32F103xBxx),y)
#FIXME: This portion of the Makefile is absolutely untested

flash_nanoboot: nanoboot.bin
	sudo stm32loader.py -wv -a 0x08000000 -p /dev/ttyUSB0 nanoboot.bin

flash_microboot: microboot.bin carveout.bin
	sudo stm32loader.py -wv -a 0x08001000 -p /dev/ttyUSB0 microboot.bin

flash_carveout: carveout.bin
	sudo stm32loader.py -wv -a 0x08002000 -p /dev/ttyUSB0 carveout.bin

flash_milliboot: milliboot.bin
	sudo stm32loader.py -wv -a 0x08001000 -p /dev/ttyUSB0

nuke_from_orbit:
	#FIXME: Need to figure it our

flash: flash_nanoboot flash_microboot flash_milliboot

endif

ifeq ($(CONFIG_STM32F107xCxx),y)
flash: flash_nanoboot flash_microboot flash_milliboot

flash_nanoboot: nanoboot.bin
	sudo dfu-util -i0 -a0 -s0x08000000 -D nanoboot.bin

flash_microboot: microboot.bin carveout.bin flash_milliboot
	sudo dfu-util -i0 -a0 -s0x08001000 -D microboot.bin

flash_milliboot: milliboot.bin carveout.bin
	cat carveout.bin milliboot.bin > blob.bin
	sudo dfu-util -i0 -a0 -s0x08002000 -D blob.bin
	rm -f blob.bin

nuke_from_orbit:
	sudo dfu-util -i0 -a0 -s0x08000000:force:unprotect:mass-erase -D nanoboot.bin
endif


assembly: nanoboot.elf
	@printf "  DISASM  $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(PREFIX)-objdump --disassemble nanoboot.elf > nanoboot.asm

assembly_milliboot: milliboot.elf
	@printf "  DISASM  $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(PREFIX)-objdump --disassemble milliboot.elf > milliboot.asm

assembly_fancyblink: fancyblink.elf
	@printf "  DISASM  $(subst $(shell pwd)/,,$(@))\n"
	$(Q)$(PREFIX)-objdump --disassemble fancyblink.elf > fancyblink.asm



clean:
	$(Q)rm -f *.bin *.elf *.o lib/*.o *.d
	$(Q)make -C tools/ clean

.PHONY: clean flash nuke_from_orbit
